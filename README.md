This patch is part of the [_META_ Environment](https://bitbucket.org/AdrianArtacho/meta4live/src/master/).

![META_logo](https://bitbucket.org/AdrianArtacho/meta4live/raw/HEAD/META_logo.png)

# META_tempo

![META:tempo](https://docs.google.com/drawings/d/e/2PACX-1vR5OYwaitwKydqjtUzWD9Wee5F45cbJpaNQc67OKlV74EJ7OVo1J5w4nJ11AkkqEY6eGdniuqWPVZvS/pub?w=226&h=346)


## Download M4L device

> [META_tempo.amxd](https://drive.google.com/file/d/1eOGqspU5oBgoDjVMXd9Lxc7tsZoMTKfe/view?usp=sharing)

## Device behavior set via Clip names

Clip name:

> `bpm 120 secs 5`

x = bpm

y = seconds

Clip name format:

> bpm xxx secs yyyy

send to receive object:

"tempo_change"

x = bpm, y = milliseconds

 BPM 154, TIME 5000, GO

---

### Set device behavior via CC 110

`CC 110` Messages can be sent ot the <META_tempo> device to control tempo, scaling the typical 0 <> 127 midi values to bpm balues between 30 bpm < value < 255 bpm:

    `110 127` = 255 bpm

More info about CC conventions within the Tesser environment cn be found [here](https://docs.google.com/spreadsheets/d/e/2PACX-1vSNPI08rSjfvctjYF4SNNbjUE4hrDPGBhdvDISSuLHciy4LdjDJawKAYrcGManydX4azAuSUnZnFZbb/pubhtml?gid=0&single=true).

---

### META_tempo

Clip name:
bpm 120 secs 5
x = bpm
y = seconds

[comment]: # (tempo change v2 by chaosmoon. contact: public@chaosmoon.info)

Clip name format:

bpm xxx secs yyyy

send to receive object:

"tempo_change"

x = bpm, y = milliseconds

 *BPM 154, TIME 5000, GO*

### Credits

tempo change v2

by chaosmoon

contact:

[public@chaosmoon.info](mailto:public@chaosmoon.info)

---

# To-Do

- Documentation
- META_tempo: receive via CC, so that I only need 1 device per set (it is also good to have the tempi in a track, because I cannot export the info in the master track!)

pattr: (BPM_Receive): failed to bind to target (BPM_receive)
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete

- when the session stops, stop also the ramp!

- allow for complex Tempo changes to be stored in clips

- ? Can I achieve those crazy Tempo changes if I'm synced, without messing up too much with the 'shared tempo'

- somehow control the "clicktrack track" from this device?

- 
